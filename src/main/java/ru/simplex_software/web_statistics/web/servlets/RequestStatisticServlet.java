package ru.simplex_software.web_statistics.web.servlets;

import ru.simplex_software.web_statistics.service.RequestStatisticService;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * Сервлет, отдающий статистику.
 */
public class RequestStatisticServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // Сброс статистики.
        if ("reset=true".equals(request.getQueryString())) {
            RequestStatisticService.resetStatistic();
            response.sendRedirect(getRequestPath(request));
        }

        // Получение статистики.
        Map<String, RequestStatisticService.RequestStatisticRecord> statistic = RequestStatisticService.getStatistic();

        // Запись статистики в ответ.
        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().print(createStatisticPage(statistic, getRequestPath(request)));
    }

    /**
     * Создание строки страницы статистики.
     *
     * @param statistic   статистика.
     * @param requestPath путь запроса.
     * @return страица статистики.
     */
    private String createStatisticPage(Map<String, RequestStatisticService.RequestStatisticRecord> statistic, String requestPath) {
        StringBuilder builder = new StringBuilder();
        builder.append("<html>");
        builder.append("<head>");
        builder.append("<title>Статистика по запросам</title>");
        builder.append("</head>");
        builder.append("<body>");
        builder.append("<a href=\"").append(requestPath).append("?reset=true\">Reset</a>");
        builder.append("<table>");
        builder.append("<tr><th>Url</th><th>Count</th><th>Min</th><th>Max</th><th>Average</th></tr>");

        for (String key : statistic.keySet()) {
            builder.append("<tr>");
            RequestStatisticService.RequestStatisticRecord record = statistic.get(key);
            String average = String.format("%.2f", record.getAverage());

            builder.append("<td>").append(key).append("</td>");
            builder.append("<td>").append(record.getCount()).append("</td>");
            builder.append("<td>").append(record.getMin()).append(" ms</td>");
            builder.append("<td>").append(record.getMax()).append(" ms</td>");
            builder.append("<td>").append(average).append(" ms</td>");
            builder.append("</tr>");
        }

        builder.append("</table></body>");
        builder.append("</html>");
        return builder.toString();
    }

    /**
     * Получение пути запроса без параметров.
     *
     * @param request запрос.
     * @return путь запроса.
     */
    private String getRequestPath(HttpServletRequest request) {
        return request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getServletPath();
    }
}