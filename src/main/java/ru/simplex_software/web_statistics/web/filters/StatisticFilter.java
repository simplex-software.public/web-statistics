package ru.simplex_software.web_statistics.web.filters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.simplex_software.web_statistics.service.RequestStatisticService;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Фильтр для замера времени выполнения запросов.
 */
public class StatisticFilter implements Filter {
    private static final Logger LOG = LoggerFactory.getLogger(StatisticFilter.class);

    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        long startTime = System.nanoTime();
        chain.doFilter(request, response);

        long endTime = System.nanoTime();
        String url = ((HttpServletRequest) request).getRequestURI();
        long elapsed = (endTime - startTime) / 1000_000;

        RequestStatisticService.addRequestData(url, elapsed);
        LOG.debug("Request to url {} executed in {} ms.", url, elapsed);
    }
}