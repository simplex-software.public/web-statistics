package ru.simplex_software.web_statistics.service;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Сервис для хранения статистики по запросам.
 */
public class RequestStatisticService {
    private static Map<String, RequestStatisticRecord> statistic = new HashMap<>();

    /**
     * Добавление данных о запросе в статистику.
     *
     * @param url           адрес запроса.
     * @param executionTime время выполнения запроса.
     */
    public static void addRequestData(String url, long executionTime) {
        RequestStatisticRecord record = statistic.getOrDefault(url, new RequestStatisticRecord());
        record.setUrl(url);
        record.setCount(record.getCount() + 1);
        record.setTotal(record.getTotal() + executionTime);

        if (executionTime < record.getMin() || record.getMin() == 0) {
            record.setMin(executionTime);
        }
        if (executionTime > record.getMax()) {
            record.setMax(executionTime);
        }
        statistic.put(url, record);
    }

    /**
     * Получение статистики по запросам.
     *
     * @return статистика по запросам.
     */
    public static Map<String, RequestStatisticRecord> getStatistic() {
        Map<String, RequestStatisticRecord> result = new LinkedHashMap<>();
        statistic.values()
                .forEach(r -> r.setAverage((float) r.getTotal() / r.getCount()));
        statistic.entrySet().stream()
                .sorted((e1, e2) -> Float.compare(e2.getValue().getAverage(), e1.getValue().getAverage()))
                //замена метода forEachOrdered на forEach
                .forEach(e -> result.put(e.getKey(), e.getValue()));
        return result;
    }

    /**
     * Сброс статистики.
     */
    public static void resetStatistic() {
        statistic.clear();
    }

    /**
     * Запись о статистике по запросу.
     */
    public static class RequestStatisticRecord {
        private String url;
        private long count;
        private long max;
        private long min;
        private long total;
        private float average;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public long getCount() {
            return count;
        }

        public void setCount(long count) {
            this.count = count;
        }

        public long getMax() {
            return max;
        }

        public void setMax(long max) {
            this.max = max;
        }

        public long getMin() {
            return min;
        }

        public void setMin(long min) {
            this.min = min;
        }

        public long getTotal() {
            return total;
        }

        public void setTotal(long total) {
            this.total = total;
        }

        public float getAverage() {
            return average;
        }

        public void setAverage(float average) {
            this.average = average;
        }
    }
}